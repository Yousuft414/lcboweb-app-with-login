package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegularAlphanumeric( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "abcd1234" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionAlphanumeric() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("######"));
	}
	
	@Test
	public void testIsValidLoginBoundaryInAlphanumeric( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "a12345" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutAlphanumeric() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("123456"));
	}
	
	@Test
	public void testIsValidLoginRegularSize( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "abcdefg" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionSize() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName(""));
	}
	
	@Test
	public void testIsValidLoginBoundaryInSize( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "aaaaaa" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutSize() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("aaaaa"));
	}

}
