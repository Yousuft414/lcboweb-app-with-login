package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return loginName.matches("^[a-zA-Z0-9]*$") && !loginName.matches("[0-9].*") && loginName.length( ) >= 6;
	}
}
